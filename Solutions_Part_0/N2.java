package Solutions_Part_0;

/*. В массиве С, размерностью 5 на 6, определить произведение элементов,
удовлетворяющих условию a<C[i][j]<b и расположенных до максимального элемента этого массива.
Значения переменных a и b вводить с клавиатуры.
*/
//правка с телефона
import java.io.*;
import java.util.*;

public class N2 {
    public static void main(String[] args) throws IOException {
        Set<Integer> element = new HashSet<>(30);
        Random random = new Random();
        int[][] C = new int[5][6];
        int max = Integer.MIN_VALUE;
        boolean bool = true;

        while (bool) {
            element.add(random.nextInt(99));
            if (element.size()==30) bool = false;
        }

        Iterator<Integer> iterator = element.iterator();
        for (int i = 0; i < 5; i++)
            for (int j = 0; j < 6; j++) {
                C[i][j] = iterator.next();
                if (C[i][j] > max) max = C[i][j];
            }

        String space = "", space1 = "|", space2 = " |";
        int I=0, J=0;
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 6; j++) {
                if (C[i][j]>9) space = space1;
                else space = space2;
                if (C[i][j]==max){I=i;J=j;}
                System.out.print(C[i][j] + space);
            }
            System.out.println();
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int off=0,before=0;
        System.out.println("\nmax =" + max + ", координаты max [" + I + "][" + J + "]"+"\n");
        System.out.println("Введите диапазон значений суммируемых элементов");
        System.out.print("больше :");
        off = Integer.parseInt(reader.readLine());
        System.out.print("меньше :");
        before = Integer.parseInt(reader.readLine());

        int sum = 0;
        exit:for (int i = 0; i < 5; i++)
            for (int j = 0; j < 6; j++) {
                if (C[i][j]==max)break exit;
                else if (C[i][j] > off && C[i][j] < before) sum = sum + C[i][j];
            }
        System.out.print("Сумма знчений элементов массива,\nв диапазоне " + off + " < элемент < " + before + "\nравна " + sum);
    }
}
