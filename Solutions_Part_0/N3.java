package Solutions_Part_0;
/*В матрице размером 6 на 9 определить сумму модулей его отрицательных нечетных элементов каждой строки.
Найти сумму элементов в тех столбцах, которые содержат хотя бы один нулевой элемент.
Заменить положительные элементы второй и третьей строк единицами.*/

import java.util.Random;

public class N3 {
    public static void main(String[] args) {
        System.out.println("               Массив");
        int[][] matrix = new int[6][9];
        Random random = new Random();

        String space = "", space1 = "|", space2 = " |",space3="  |";
        int[] sum = new int[6];

        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 9; j++) {
                matrix[i][j] = random.nextInt(99) - 20;
                if (matrix[i][j]<0 && (matrix[i][j]%2)!=0)sum[i]=sum[i]+Math.abs(matrix[i][j]);
                if (matrix[i][j]<0 && Math.abs(matrix[i][j])<10) space = space2;
                else if (matrix[i][j]<0 && Math.abs(matrix[i][j])>=10) space = space1;
                else if (matrix[i][j]<10) space = space3;
                else if (matrix[i][j]>10) space = space2;
                System.out.print(matrix[i][j] + space);
            }
            System.out.println();
        }

        System.out.println("число возле стрелки указывает номер столбца в котором есть \"0\"");
        int[] column = new int[9];

        for (int j = 0; j < 9; j++)
            for (int i = 0; i< 6; i++) {
                if (matrix[i][j]==0) {column[j] = j;break;}
            }

        for (int j = 0; j < 9; j++) System.out.print(" \u2191"+column[j]+" ");
        System.out.println("\n");

        for (int i=0;i<6;i++) {
            System.out.println("сумма модулей отрицательных нечетных элементов строки " + i + ": =" + sum[i]);
        }

        int[] sumColumnWithNull = new int[9];
        for (int j = 0; j < 9; j++) {
            if (column[j]==0)continue;
            for (int i = 0; i < 6; i++) {
                sumColumnWithNull[j] = sumColumnWithNull[j] + matrix[i][j];
            }
        }
        System.out.println();

        for (int i=0;i<9;i++) {
            if (column[i]==0)continue;
            System.out.println("столбец "+i+" содержит нулевой элемент, сумма элементов =" + sumColumnWithNull[i]);
        }
        int nul = 0;
        for (int i = 0; i < column.length; i++) nul += column[i];
        if (nul==0) System.out.println("Массив не содержит нулевого значения");
        System.out.println();

        System.out.println("Массив после замены положительных элементов второй и третьей строк единицами.");
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 9; j++) {
                if (matrix[2][j]>=0) matrix[2][j] = 1;
                if (matrix[3][j]>=0) matrix[3][j] = 1;

                if (matrix[i][j]<0 && (matrix[i][j]%2)!=0)sum[i]=sum[i]+Math.abs(matrix[i][j]);
                if (matrix[i][j]<0 && Math.abs(matrix[i][j])<10) space = space2;
                else if (matrix[i][j]<0 && Math.abs(matrix[i][j])>=10) space = space1;
                else if (matrix[i][j]<10) space = space3;
                else if (matrix[i][j]>10) space = space2;
                System.out.print(matrix[i][j] + space);
            }
            System.out.println();
        }

    }
}
